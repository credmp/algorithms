package nl.arjenwiersma.dijkstra;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;


/**
 * DijkstraTest
 */
public class DijkstraTest {

    @Test
    public void sampleGraph() {
        Vertex vertexA = new Vertex("A");
        Vertex vertexB = new Vertex("B");
        Vertex vertexC = new Vertex("C");
        Vertex vertexD = new Vertex("D");
        Vertex vertexE = new Vertex("E");
        Vertex vertexF = new Vertex("F");

        vertexA.addNeighbor(vertexB, 2);
        vertexA.addNeighbor(vertexC, 8);
        vertexB.addNeighbor(vertexC, 5);
        vertexB.addNeighbor(vertexD, 6);
        vertexB.addNeighbor(vertexE, 3);
        vertexC.addNeighbor(vertexE, 1);
        vertexD.addNeighbor(vertexF, 2);
        vertexE.addNeighbor(vertexD, 4);
        vertexE.addNeighbor(vertexF, 7);

        Dijkstra shortestPath = new Dijkstra();
        shortestPath.calculate(vertexA);

        assertEquals(2, vertexB.getDistance());
        assertEquals(7, vertexC.getDistance());
        assertEquals(8, vertexD.getDistance());
        assertEquals(5, vertexE.getDistance());
        assertEquals(10, vertexF.getDistance());

        assertEquals(List.of(vertexA, vertexB), shortestPath.getPathTo(vertexB));
        assertEquals(List.of(vertexA, vertexB, vertexC), shortestPath.getPathTo(vertexC));
        assertEquals(List.of(vertexA, vertexB, vertexD), shortestPath.getPathTo(vertexD));
        assertEquals(List.of(vertexA, vertexB, vertexE), shortestPath.getPathTo(vertexE));
        assertEquals(List.of(vertexA, vertexB, vertexD, vertexF), shortestPath.getPathTo(vertexF));

    }
}
