package nl.arjenwiersma.tgg;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TGGInfixToPostfixExampleTest {

    @Test
    public void testInfixToPostfix() {
        // Source (infix) Graph: "3 + 4"
        Graph infixGraph = new Graph(new Node[]{new Node("3"), new Node("+"), new Node("4")});

        // Correspondence Graph
        Correspondence[] corrGraph = new Correspondence[3];

        // Apply the TGG rule to create the target (postfix) graph
        Graph postfixGraph = TGGInfixToPostfixExample.infixToPostfix(infixGraph, corrGraph);

        // Verify the postfix expression: "3 4 +"
        assertEquals("3", postfixGraph.nodes[0].value);
        assertEquals("4", postfixGraph.nodes[1].value);
        assertEquals("+", postfixGraph.nodes[2].value);
    }

    @Test
    public void testGraph() {
        // Source (infix) Graph: "3 + 4"
        Graph infixGraph = new Graph(new Node[]{new Node("3"), new Node("+"), new Node("4")});

        // Correspondence Graph
        Correspondence[] corrGraph = new Correspondence[3];

        // Apply the TGG rule to create the target (postfix) graph
        Graph postfixGraph = TGGInfixToPostfixExample.infixToPostfix(infixGraph, corrGraph);

        // Output the source and target expressions
        System.out.print("Infix: ");
        for(Node node : infixGraph.nodes) {
            System.out.print(node.value + " ");
        }
        System.out.println();

        System.out.print("Postfix: ");
        for(Node node : postfixGraph.nodes) {
            System.out.print(node.value + " ");
        }
        System.out.println();
    }
}
