package nl.arjenwiersma.dijkstra;

public record Edge(int weight, Vertex targetVertex) {}
