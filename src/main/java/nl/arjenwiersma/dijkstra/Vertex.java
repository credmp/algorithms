package nl.arjenwiersma.dijkstra;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * A vertex in a graph
 */
public class Vertex implements Comparable<Vertex> {
    // Name of the vertex
    private String name;
    // List of edges that connect this vertex to another vertex
    private List<Edge> adjacenciesList;
    // Has the vertex been visited yet?
    private boolean visited;
    // The vertex that has the shortest path to this vertex
    private Vertex predecessor;
    // The distance to this vertex through the predecessor
    private int distance = Integer.MAX_VALUE;

    public Vertex(String name) {
        this.name = name;
        this.adjacenciesList = new ArrayList<>();
    }

    public void addNeighbor(Vertex neighbor, int weight) {
        this.adjacenciesList.add(new Edge(weight, neighbor));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Edge> getAdjacenciesList() {
        return adjacenciesList;
    }

    public void setAdjacenciesList(List<Edge> adjacenciesList) {
        this.adjacenciesList = adjacenciesList;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public Vertex getPredecessor() {
        return predecessor;
    }

    public void setPredecessor(Vertex predecessor) {
        this.predecessor = predecessor;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public int compareTo(Vertex otherVertex) {
        return Integer.compare(this.distance, otherVertex.getDistance());
    }

    public Stream<Vertex> flattened() {
        return Stream.concat(Stream.of(this), predecessor != null ? predecessor.flattened() : Stream.empty());
    }
}
