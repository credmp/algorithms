package nl.arjenwiersma.dijkstra;

import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Collectors;

public class Dijkstra {
    public void calculate(Vertex start) {
        start.setDistance(0);

        Queue<Vertex> queue = new PriorityQueue<>() {{
                add(start);
        }};

        while (!queue.isEmpty()) {
            /**
             * Get the vertex from the queue with the least amount of distance
             */
            Vertex current = queue.poll();

            /**
             * For each of the edges from this vertex
             */
            for (Edge edge : current.getAdjacenciesList()) {

                /**
                 * Examine the vertex if it has not been visited yet
                 */
                Vertex v = edge.targetVertex();
                if (!v.isVisited()) {
                    /**
                     * Calculate the distance to get to the vertex from the current vertex
                     */
                    int newDistance = current.getDistance() + edge.weight();

                    /**
                     * If the distance is less then the known distance
                     */
                    if (newDistance < v.getDistance()) {
                        // Remove it from the queue, if it was already on the queue
                        queue.remove(v);
                        // Set the new distance for the vertex
                        v.setDistance(newDistance);
                        // The current vertex is the shortest path to this vertex
                        v.setPredecessor(current);
                        // Add the vertex to the queue, recalculating its priority
                        queue.add(v);
                    }
                }
            }
            /**
             * Mark the current vertex as visited and fully examined
             */
            current.setVisited(true);
        }
    }

    /**
     * Retrieve the path to the destination {@link Vertex}, in the order from the
     * start {@link Vertex}.
     */
    public List<Vertex> getPathTo(Vertex destination) {
        return destination.flattened().collect(Collectors.collectingAndThen(Collectors.toList(), l -> {
            Collections.reverse(l);
            return l;
        }));        
    }
}
