package nl.arjenwiersma.tgg;

class Node {
    String value;
    Node(String value) { this.value = value; }
}

class Graph {
    Node[] nodes;
    Graph(Node[] nodes) { this.nodes = nodes; }
}

class Correspondence {
    Node sourceNode;
    Node targetNode;
    Correspondence(Node sourceNode, Node targetNode) {
        this.sourceNode = sourceNode;
        this.targetNode = targetNode;
    }
}

public class TGGInfixToPostfixExample {
    public static Graph infixToPostfix(Graph infixGraph, Correspondence[] corrGraph) {
        // Create a new postfix graph
        Node[] postfixNodes = new Node[infixGraph.nodes.length];
        for(int i = 0; i < infixGraph.nodes.length; i++) {
            postfixNodes[i] = new Node(infixGraph.nodes[i].value);
        }
        Graph postfixGraph = new Graph(postfixNodes);

        // Create correspondence between each node in the infix and postfix graphs
        for(int i = 0; i < infixGraph.nodes.length; i++) {
            corrGraph[i] = new Correspondence(infixGraph.nodes[i], postfixGraph.nodes[i]);
        }

        // Assume the infix graph is structured as: Operand Operator Operand
        // Reorder nodes in the postfix graph to: Operand Operand Operator
        postfixGraph.nodes[1] = new Node(infixGraph.nodes[2].value);
        postfixGraph.nodes[2] = new Node(infixGraph.nodes[1].value);

        // Update the correspondence graph
        corrGraph[1].targetNode = postfixGraph.nodes[1];
        corrGraph[2].targetNode = postfixGraph.nodes[2];

        return postfixGraph;
    }

    public static void main(String[] args) {
        // Source (infix) Graph: "3 + 4"
        Graph infixGraph = new Graph(new Node[]{new Node("3"), new Node("+"), new Node("4")});

        // Correspondence Graph
        Correspondence[] corrGraph = new Correspondence[3];

        // Apply the TGG rule to create the target (postfix) graph
        Graph postfixGraph = infixToPostfix(infixGraph, corrGraph);

        // Output the source and target expressions
        System.out.print("Infix: ");
        for(Node node : infixGraph.nodes) {
            System.out.print(node.value + " ");
        }
        System.out.println();

        System.out.print("Postfix: ");
        for(Node node : postfixGraph.nodes) {
            System.out.print(node.value + " ");
        }
        System.out.println();
    }
}
